<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AlbumTrack;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Albumtrack controller.
 *
 * @Route("albumtrack")
 */
class AlbumTrackController extends Controller
{
    /**
     * Lists all albumTrack entities.
     *
     * @Route("/", name="albumtrack_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $albumTracks = $em->getRepository('AppBundle:AlbumTrack')->findAll();

        return $this->render('albumtrack/index.html.twig', array(
            'albumTracks' => $albumTracks,
        ));
    }
	
	/**
	 * Creates a new albumTrack entity.
	 *
	 * @Route("/new", name="albumtrack_new")
	 * @Method({"GET", "POST"})
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
    public function newAction(Request $request)
    {
        $albumTrack = new Albumtrack();
        $form = $this->createForm('AppBundle\Form\AlbumTrackType', $albumTrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($albumTrack);
            $em->flush();

            return $this->redirectToRoute('albumtrack_show', array('id' => $albumTrack->getId()));
        }

        return $this->render('albumtrack/new.html.twig', array(
            'albumTrack' => $albumTrack,
            'form' => $form->createView(),
        ));
    }
	
	/**
	 * Finds and displays a albumTrack entity.
	 *
	 * @Route("/{id}", name="albumtrack_show")
	 * @Method("GET")
	 * @param AlbumTrack $albumTrack
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
    public function showAction(AlbumTrack $albumTrack)
    {
        $deleteForm = $this->createDeleteForm($albumTrack);

        return $this->render('albumtrack/show.html.twig', array(
            'albumTrack' => $albumTrack,
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	/**
	 * Displays a form to edit an existing albumTrack entity.
	 *
	 * @Route("/{id}/edit", name="albumtrack_edit")
	 * @Method({"GET", "POST"})
	 * @param Request $request
	 * @param AlbumTrack $albumTrack
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
    public function editAction(Request $request, AlbumTrack $albumTrack)
    {
        $deleteForm = $this->createDeleteForm($albumTrack);
        $editForm = $this->createForm('AppBundle\Form\AlbumTrackType', $albumTrack);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('albumtrack_edit', array('id' => $albumTrack->getId()));
        }

        return $this->render('albumtrack/edit.html.twig', array(
            'albumTrack' => $albumTrack,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
	
	/**
	 * Deletes a albumTrack entity.
	 *
	 * @Route("/{id}", name="albumtrack_delete")
	 * @Method("DELETE")
	 * @param Request $request
	 * @param AlbumTrack $albumTrack
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
    public function deleteAction(Request $request, AlbumTrack $albumTrack)
    {
        $form = $this->createDeleteForm($albumTrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($albumTrack);
            $em->flush();
        }

        return $this->redirectToRoute('albumtrack_index');
    }

    /**
     * Creates a form to delete a albumTrack entity.
     *
     * @param AlbumTrack $albumTrack The albumTrack entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AlbumTrack $albumTrack)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('albumtrack_delete', array('id' => $albumTrack->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
