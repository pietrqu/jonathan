<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Form\Custom\AlbumType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/custom")
 */
class CustomController extends Controller
{
    /**
     * @Route("", name="app_custom_list")
     */
    public function listAction()
    {
        return $this->render('custom/list.html.twig', [
            'albums' => $this->getDoctrine()->getRepository(Album::class)->findAll(),
        ]);
    }
	
	/**
	 * @Route("/update/{id}", name="app_custom_tracks")
	 * @param Request $request
	 * @param Album $album
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
    public function tracksAction(Request $request, Album $album)
    {
        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('app_custom_list');
        }

        return $this->render('custom/update.html.twig', [
            'form' => $form->createView(),
            'album' => $album,
        ]);
    }
}
