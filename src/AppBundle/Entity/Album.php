<?php


namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

//* An album is related to several tracks, but tracks have some extra properties - like position - so OneToMany

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class Album
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	private $albumName;
	
	/**
	 * @var integer
	 * @ORM\Column(type="string")
	 */
	private $position;
	
	/**
	 * @ORM\OneToMany(targetEntity="AlbumTrack", mappedBy="album", cascade={"all"}, orphanRemoval=true)
	 */
	private $relatedTracks;
	
	public function __construct()
	{
		$this->relatedTracks = new ArrayCollection();
	}
	
	public function getRelatedTracks()
	{
		return $this->relatedTracks;
	}
	
	public function setRelatedTracks($tracks)
	{
		$this->relatedTracks = $tracks;
	}
	
	public function addRelatedTrack($track)
	{
		if (!$this->relatedTracks->contains($track)) {
			$this->relatedTracks->add($track);
			$track->setAlbum($this);
		}
	}

	public function removeRelatedTrack($track)
	{
		if ($this->relatedTracks->contains($track)) {
			$this->relatedTracks->removeElement($track);
			$track->setAlbum(null);
		}
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @return mixed
	 */
	public function getAlbumName()
	{
		return $this->albumName;
	}
	
	/**
	 * @param mixed $albumName
	 */
	public function setAlbumName($albumName)
	{
		$this->albumName = $albumName;
	}
	
	/**
	 * @return int
	 */
	public function getPosition()
	{
		return $this->position;
	}
	
	/**
	 * @param int $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}
	
	public function __toString()
	{
		return (string)$this->id;
	}
}