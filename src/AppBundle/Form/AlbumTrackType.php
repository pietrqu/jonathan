<?php

namespace AppBundle\Form;

use AppBundle\Entity\Album;
use AppBundle\Entity\AlbumTrack;
use AppBundle\Entity\Track;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumTrackType extends AbstractType
//{
//    /**
//     * {@inheritdoc}
//     */
//    public function buildForm(FormBuilderInterface $builder, array $options)
//    {
//        $builder->add('track')->add('album')->add('position');
//    }/**
//     * {@inheritdoc}
//     */
//    public function configureOptions(OptionsResolver $resolver)
//    {
//        $resolver->setDefaults(array(
//            'data_class' => 'AppBundle\Entity\AlbumTrack'
//        ));
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix()
//    {
//        return 'appbundle_albumtrack';
//    }
//
//
//}
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'track',
				EntityType::class,
				[
					'class'        => Track::class,
					'choice_label' => 'trackName',
					'multiple' =>true,
					'expanded' =>false,
				]
			)
			->add(
				'album',
				EntityType::class,
				[
					'class'        => Album::class,
					'choice_label' => 'albumName',
				]
			)
			->add('position', IntegerType::class);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefault('data_class', AlbumTrack::class);
	}
	
	public function getBlockPrefix()
	{
		return 'app_bundle_album_track_type';
	}
}