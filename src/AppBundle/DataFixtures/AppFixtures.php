<?php


namespace AppBundle\DataFixtures;


use AppBundle\Entity\Album;
use AppBundle\Entity\Track;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
	public function load(ObjectManager $manager)
	{
		for ($i = 0 ; $i < 20 ; $i++) {
			$album = new Album();
			$album->setAlbumName('Album #'.rand(10, 100));
			$album->setPosition($i);
			
			$manager->persist($album);
		}
		
		for ($i = 0 ; $i < 30 ; $i++) {
			$tracks = new Track();
			$tracks->setTrackName('Track #'.rand(10, 100));
			
			$manager->persist($tracks);
		}
		
		$manager->flush();
	}
	
}
    